/* gcc -o test ini_auth.c ../ini.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../ini.h"

#define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0

typedef struct 
{
    const char* super_account;
    const char* super_password;
    const char* normal_account;
    const char* normal_password;
} auth_t;

static int auth_parser(void* data, const char* section, const char* name, const char* value)
{
    auth_t* pconfig = (auth_t*)data;

    if (MATCH("auth", "super_account")) 
    {
        pconfig->super_account = strdup(value);
    } 
    else if (MATCH("auth", "super_password")) 
    {
        pconfig->super_password = strdup(value);
    } 
    else if (MATCH("auth", "normal_account")) 
    {
        pconfig->normal_account = strdup(value);
    } 
    else if (MATCH("auth", "normal_password")) {
        pconfig->normal_password = strdup(value);
    } 
    else 
    {
        return 0;  /* unknown section/name, error */
    }
    return 1;
}

int main(int argc, char* argv[])
{
    auth_t auth;

    if (ini_parse("./auth.ini", auth_parser, &auth) < 0) {
        printf("Can't load 'auth.ini'\n");
        return 1;
    }
    printf("super account: %s, password: %s\nnormal account: %s, password: %s\n",
        auth.super_account, auth.super_password, auth.normal_account, auth.normal_password);
    return 0;
}
