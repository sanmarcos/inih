/* gcc -o test ini_brweb.c ../ini.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../ini.h"

#define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0

typedef struct 
{
    //log section
    const char* syslog;
    const char* access_log_file;
    const char* error_log_file;

    //auth section
    int ttl;
    int num_sessions;

    //server section
    const char* document_root;
    const char* listening_ports;
    const char* index_files;
    const char* ssl_certificate;
    int num_threads;
    int request_timeout_ms;
    const char* enable_keep_alive;
    const char* enable_directory_listing;
    const char* hide_files_patterns;
    const char* throttle;
    const char* access_control_list;
    const char* extra_mime_types;

} civet_t;

static int server_conf_parser(void* data, const char* section, const char* name, const char* value)
{
    civet_t* pconfig = (civet_t*)data;

    //log section
    if (MATCH("log", "syslog")) 
    {
        pconfig->syslog = strdup(value);
    } 
    else if (MATCH("log", "access_log_file")) 
    {
        pconfig->access_log_file = strdup(value);
    } 
    else if (MATCH("log", "error_log_file")) 
    {
        pconfig->error_log_file = strdup(value);
    } 
    // auth section
    else if (MATCH("auth", "ttl")) {
        pconfig->ttl = atoi(value);
    } 
    else if (MATCH("auth", "num_sessions")) {
        pconfig->num_sessions = atoi(value);
    }

    //server
    else if (MATCH("server", "document_root")) 
    {
        pconfig->document_root = strdup(value);
    }
    else if (MATCH("server", "listening_ports")) 
    {
        pconfig->listening_ports = strdup(value);
    }
    else if (MATCH("server", "index_files")) 
    {
        pconfig->index_files = strdup(value);
    }
    else if (MATCH("server", "ssl_certificate")) 
    {
        pconfig->ssl_certificate = strdup(value);
    }
    else if (MATCH("server", "num_threads")) 
    {
        pconfig->num_threads = atoi(value);
    }
    else if (MATCH("server", "request_timeout_ms")) 
    {
        pconfig->request_timeout_ms = atoi(value);
    }
    else if (MATCH("server", "enable_keep_alive")) 
    {
        pconfig->enable_keep_alive = strdup(value);
    }
    else if (MATCH("server", "enable_directory_listing")) 
    {
        pconfig->enable_directory_listing = strdup(value);
    }
    else if (MATCH("server", "hide_files_patterns")) 
    {
        pconfig->hide_files_patterns = strdup(value);
    }
    else if (MATCH("server", "throttle")) 
    {
        pconfig->throttle = strdup(value);
    }
    else if (MATCH("server", "access_control_list")) 
    {
        pconfig->access_control_list = strdup(value);
    }
    else if (MATCH("server", "extra_mime_types")) 
    {
        pconfig->extra_mime_types = strdup(value);
    }
    else 
    {
        return 0;  /* unknown section/name, error */
    }
    return 1;
}

int main(int argc, char* argv[])
{
    civet_t civet_setting =  {
        .syslog                     = NULL,
        .access_log_file            = NULL, 
        .error_log_file             = NULL,
        .ttl                        = -1,
        .num_sessions               = -1,
        .document_root              = NULL,
        .listening_ports            = NULL,
        .index_files                = NULL,
        .ssl_certificate            = NULL,
        .num_threads                = -1,
        .request_timeout_ms         = -1,
        .enable_keep_alive          = NULL,
        .enable_directory_listing   = NULL,
        .hide_files_patterns        = NULL,
        .throttle                   = NULL,
        .access_control_list        = NULL,
        .extra_mime_types           = NULL
    };

    if (ini_parse("./brweb.ini", server_conf_parser, &civet_setting) < 0) {
        printf("Can't load 'brweb.ini'\n");
        return 1;
    }

    printf("\n[Log]\n=====\nEnable syslog: %s\naccess log:%s\nerror log:%s\n",
        civet_setting.syslog, civet_setting.access_log_file, civet_setting.error_log_file);
    printf("\n[Auth]\n=====\nttl: %d\nnum_sessions: %d\n", 
        civet_setting.ttl, civet_setting.num_sessions);
    printf("\n[server]\n=====\ndocument_root: %s\nlistening_ports: %s\nindex_files: %s\n", 
        civet_setting.document_root, civet_setting.listening_ports, civet_setting.index_files);
    printf("ssl_certificate: %s\nnum_threads: %d\nrequest_timeout_ms: %d\n",
        civet_setting.ssl_certificate, civet_setting.num_threads, civet_setting.request_timeout_ms);
    printf("enable_keep_alive: %s\nenable_directory_listing: %s\nhide_files_patterns: %s\n",
        civet_setting.enable_keep_alive, civet_setting.enable_directory_listing, civet_setting.hide_files_patterns);
    printf("throttle: %s\naccess_control_list: %s\nextra_mime_types: %s\n\n",
        civet_setting.throttle, civet_setting.access_control_list, civet_setting.extra_mime_types);

    return 0;
}
